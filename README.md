This repository collects experiments for rir.

To create a reproducible experiment add a new folder to experiments and put a dockerfile there.
Use the following template

    ARG RIR_VERSION
    FROM registry.gitlab.com/rirvm/rir_mirror/benchmark:$RIR_VERSION
    ADD yourfiles /opt/
    ENTRYPOINT /opt/yourexperiment

The `RIR_VERSION` variable is set to a fixed git commit id of rir.
It is pinned in `buildContainers.sh`.

Once you push to this repo a container is built and published to the registry with a tag that consists of `RIR_VERSION-THIS_REPO_COMMIT_ID`.

To run an experiment use

    docker run registry.gitlab.com/rirvm/rir_experiments/experiment_name:RIR_VERSION-THIS_REPO_COMMIT_ID

See [the registry](https://gitlab.com/rirvm/rir_experiments/container_registry) for the list of available experiments.
