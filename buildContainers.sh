#!/bin/sh

RIR_VERSION="57f375054404c4f1f45519d5b5dfd9ccd7a97282"

for experiment in `ls experiments`; do
  cd experiments/$experiment
  docker build --build-arg RIR_VERSION=$RIR_VERSION -t registry.gitlab.com/rirvm/rir_experiments/$experiment:$RIR_VERSION-$CI_COMMIT_SHA .
  docker push registry.gitlab.com/rirvm/rir_experiments/$experiment:$RIR_VERSION-$CI_COMMIT_SHA
  cd ../..
done
